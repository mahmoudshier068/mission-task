import {Platform} from 'react-native';

export default {
  normal: 'ArminGrotesk-Normal',
  bold:
    Platform.OS == 'ios' ? 'ArminGrotesk-SemiBoldItalic' : 'SF-Pro-Text-Bold',
  boldIsStyle: true,
};
