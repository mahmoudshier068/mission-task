import React from 'react';
import Spinner from 'react-native-spinkit';

import {responsiveFontSize, moderateScale} from './utils/responsiveDimensions';
import {getThemeColor} from './utils/colors';

const Indicator = props => {
  const {color, mv, style, type} = props;
  var color1 = getThemeColor(color);
  let {size} = props;
  // if (Platform.OS === 'ios') {
  //   size = 'small';
  // } else {
  size = responsiveFontSize(size);
  // }

  return (
    <Spinner
      style={[style, {marginVertical: mv ? moderateScale(mv) : 0}]}
      isVisible={true}
      size={size}
      type={type ? type : 'Bounce'}
      color={color1}
    />
  );
};
export default Indicator;
