import React from 'react';
import {Text as NativeText} from 'react-native';
import {useSelector} from 'react-redux';
import {
  backgroundColorStyles,
  borderRadiusStyles,
  borderStyles,
  colorStyles,
  fontFamilyStyles,
  fontSizeStyles,
  marginStyles,
  paddingStyles,
  selfLayoutStyles,
  textDirectionStyles,
} from './Base';
// import {convertNumbers} from './utils/numbers';

const Text = props => {
  const {
    children,
    numberOfLines,
    enableReadMore,
    readMoreView,
    readLessView,
    style,
    translateNumbers,
    stopTranslateNumbers,
    ...rest
  } = props;
  const rtl = false;
  return (
    <NativeText
      {...rest}
      style={[
        selfLayoutStyles(props),
        textDirectionStyles(props),
        fontFamilyStyles(props),
        fontSizeStyles(props),
        colorStyles(props),
        backgroundColorStyles(props),
        borderStyles(props),
        borderRadiusStyles(props),
        paddingStyles(props),
        marginStyles(props),
        props.style,
      ]}
      numberOfLines={numberOfLines}>
      {props.children}
    </NativeText>
  );
};

export default Text;
