import {getColors} from '../Theme';

export const getThemeColor = color => {
  for (var c of Object.keys(getColors())) {
    if (color === c) {
      return getColors()[c];
    }
  }
  return color;
};
