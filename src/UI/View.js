import React, {PureComponent} from 'react';
import {
  View as NativeView,
  TouchableOpacity,
  Text,
  Pressable,
} from 'react-native';
import {RectButton} from 'react-native-gesture-handler';
// import LinearGradient from 'react-native-linear-gradient';
import {
  BasePropTypes,
  dimensionsStyles,
  selfLayoutStyles,
  childrenLayoutStyles,
  backgroundColorStyles,
  elevationStyles,
  paddingStyles,
  marginStyles,
  borderStyles,
  borderRadiusStyles,
} from './Base';
import {useSelector} from 'react-redux';
// import Navigation from './Navigation';

const View = props => {
  
  props = {...props, };
  const {
    onLayout,
    style,
    onPress,
    touchableOpacity,
    children,
    disabled,
    // voice,
    gradient,
    gColors,
    noFlex,
    shouldLogin,
  } = props;
  // const Container = touchableOpacity ? TouchableOpacity : RectButton;
  const Container = TouchableOpacity;
  const node = onPress ? (
    <Container
      onPress={
        !disabled
          ? () => {
              if (shouldLogin) {
                // Navigation.push({
                //   name: 'Login',
                //   passProps: {shouldLogin: true},
                // });
              } else onPress();
            }
          : null
      }
      activeOpacity={disabled ? 0.2 : 0.2}
      style={[
        childrenLayoutStyles(props),
        paddingStyles(props),
        noFlex ? null : {flex: 1, alignSelf: 'stretch'},
      ]}
      // style={[
      //   dimensionsStyles(props),
      //   selfLayoutStyles(props),
      //   childrenLayoutStyles(props),
      //   backgroundColorStyles(props),
      //   elevationStyles(props),
      //   paddingStyles(props),
      //   marginStyles(props),
      //   borderStyles(props),
      //   borderRadiusStyles(props),
      //   style,
      // ]}
    >
      {children}
    </Container>
  ) : (
    <>{children}</>
  );

  return (
    <NativeView
      onLayout={onLayout}
      onPress={props.onPress}
      style={[
        dimensionsStyles(props),
        selfLayoutStyles(props),
        childrenLayoutStyles(props),
        backgroundColorStyles(props),
        elevationStyles(props),
        paddingStyles(props),
        marginStyles(props),
        borderStyles(props),
        borderRadiusStyles(props),
        style,
      ]}>
      <>{node}</>
    </NativeView>
  );
};
export default View;
