import React, {PureComponent} from 'react';
import {Platform} from 'react-native';
import {useSelector} from 'react-redux';

import {getIconType, getIconSizeScaleFix} from './utils/icon';

import {
  BasePropTypes,
  fontSizeStyles,
  colorStyles,
  backgroundColorStyles,
  borderRadiusStyles,
  paddingStyles,
  marginStyles,
  elevationStyles,
} from './Base';

const Icon = props => {
  const rtl = false;
 
  const flipDirectionStyles = () => {
    const {flip, reverse} = props;
    if ((flip && rtl) || (reverse && !rtl)) {
      return {
        transform: [{scaleX: -1}],
      };
    }
    return null;
  };

  const {type, name, style, logo, ...rest} = props;
  const NativeIcon = getIconType(type);

  const prefix =
    type === 'ion' && !logo ? (Platform.OS === 'ios' ? 'ios-' : 'md-') : '';

  return (
    <NativeIcon
      {...rest}
      name={prefix + name}
      style={[
        flipDirectionStyles(),
        fontSizeStyles(props, getIconSizeScaleFix(type)),
        colorStyles(props),
        backgroundColorStyles(props),
        borderRadiusStyles(props),
        paddingStyles(props),
        marginStyles(props),
        elevationStyles(props),
        style,
        {
          textAlignVertical: 'center',
          textAlign: 'center',
        },
      ]}
    />
  );
};

export default Icon;
