// import './Base/polyfill';

export {default as View} from './View';
export {default as Text} from './Text';
export {default as Icon} from './Icon';
export {default as Indicator} from './Indicator';
export {getColors, getTheme, getFonts} from './Theme';
export {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
  moderateScale,
} from './utils/responsiveDimensions';
