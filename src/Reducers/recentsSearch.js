import {ADD_RECENT} from '../Action/types';

const initialState = {
  allRecentsSearch: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_RECENT: {
      return {
        ...state,
        allRecentsSearch: [...state.allRecentsSearch, action.payload],
      };
    }

    default:
      return state;
  }
  r;
};

export default reducer;
