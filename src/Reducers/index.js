import networkReducer from './network';
import RecentsSearchReducer from './recentsSearch';

import {combineReducers} from 'redux';

export default combineReducers({
  network: networkReducer,
  recentsSearch: RecentsSearchReducer,
});
