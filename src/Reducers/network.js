import {SET_INTERNET_CONNECTION} from '../Action/types';

const initialState = {
  isConnected: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_INTERNET_CONNECTION:
      return {...state, isConnected: action.payload};
    default:
      return state;
  }
};

export default reducer;
