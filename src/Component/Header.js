import React, {createRef, useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
// import {View} from 'react-native';
import {
  View,
  Icon,
  responsiveFontSize,
  responsiveWidth,
  Indicator,
  Text,
} from '@UI';
import {View as RNView, Pressable, BackHandler} from 'react-native';
import {I18nManager, Keyboard} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import * as Animatable from 'react-native-animatable';
import useSearch from '../Logic/useSearch';

const Header = props => {
  const searchInputRef = useRef();
  let {loading, valueSearch, setValueSearch} = props;
  function handleBackButtonClick() {
    // if()
    if (props.searchActive) {
      Keyboard.dismiss();
      props.setSearchActive(false);
    }
    // else navigation.goBack();
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);

  return (
    <View backgroundColor="primary" center pv={props.searchActive ? 2 : null}>
      {!props.searchActive && (
        <Animatable.View
          circleRadius={10}
          center
          mh={3}
          mv={2}
          style={{
            width: responsiveWidth(100),
            alignItems: 'center',
            justifyContent: 'center',
          }}
          animation={'fadeInDown'}
          duration={500}>
          <Text color="white" size={10} centerSelfe>
            {props.title}
          </Text>
        </Animatable.View>
      )}
      <View row ph={3} pb={2}>
        {props.searchActive && (
          <Animatable.View
            circleRadius={10}
            center
            mh={3}
            mv={2}
            style={{
              width: responsiveWidth(10),
              height: responsiveWidth(10),
              borderRadius: responsiveWidth(5),
              alignItems: 'center',
              justifyContent: 'center',
            }}
            animation={I18nManager.isRTL ? 'fadeInLeft' : 'fadeInRight'}
            duration={200}>
            <View
              circleRadius={10}
              center
              onPress={() => {
                props.setSearchActive(false);
                Keyboard.dismiss();
              }}>
              <Icon name="arrow-back" type="Ionicons" color="white" size={12} />
            </View>
          </Animatable.View>
        )}
        <View
          backgroundColor="white"
          spaceBetween
          mh={2}
          mv={2}
          borderRadius={4}
          row
          flex
          height={6}
          stretch>
          <Icon
            name="search"
            size={10}
            color="primary"
            type="FontAwesome"
            mh={3}
          />
          <TextInput
            selectionColor="#AEBBD3"
            allowFontScaling
            numberOfLines={1}
            style={{fontSize: responsiveFontSize(8), flex: 1}}
            onChangeText={text => {
              setValueSearch(text);
            }}
            placeholder={'Search'}
            ref={searchInputRef}
            onFocus={() => {
              props.setSearchActive(true);
            }}
          />
          {valueSearch && !loading ? (
            <View
              circleRadius={10}
              center
              mh={1}
              onPress={() => {
                searchInputRef.current.clear();
                setValueSearch('');
              }}>
              <Icon size={10} color="primary" name="close" type="ant" />
            </View>
          ) : valueSearch && loading ? (
            <View mh={5}>
              <Indicator size={5} color="black" mh={5} />
            </View>
          ) : null}
        </View>
      </View>
    </View>
  );
};

Header.propTypes = {
  isLoading: PropTypes.bool,
};
export default Header;
