import axios from 'axios';
import React, {useState} from 'react';
import {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {API_KEY, BASE_URL} from '../Config';

const useSearch = () => {
  const [loading, setLoading] = useState(false);
  const [valueSearch, setValueSearch] = useState(null);
  const [allAutoComplete, setAllAutoComplete] = useState([]);
  const [allGift, setAllGift] = useState([]);
  const [page, setPage] = useState(1);

  const getDataFromApiAutoComplete = () => {
    setLoading(true);
    axios
      .get(BASE_URL + '/gifs/trending', {
        params: {api_key: API_KEY, q: valueSearch, limit: 5},
      })
      .then(response => {
        setLoading(false);

        setAllAutoComplete(response.data.data);
      })
      .catch(error => {
        setLoading(false);
      });
  };

  const fetchGifts = () => {
    setLoading(true);
    axios
      .get(BASE_URL + '/gifs/trending', {
        params: {api_key: API_KEY, limit: page * 10},
      })
      .then(response => {
        setLoading(false);

        setAllGift(response.data.data);
      })
      .catch(error => {
        setLoading(false);
      });
  };

  useEffect(() => {
    fetchGifts();
  }, [page]);

  useEffect(() => {
    setAllAutoComplete([]);
    const delayDebounceFn = setTimeout(() => {
      if (valueSearch) getDataFromApiAutoComplete();
    }, 500);
    return () => clearTimeout(delayDebounceFn);
  }, [valueSearch]);

  return {
    loading,
    valueSearch,
    setValueSearch,
    allAutoComplete,
    allGift,
    page,
    setPage,
  };
};

export default useSearch;
