import React, {useEffect, useState} from 'react';
import {FlatList} from 'react-native-gesture-handler';
import {
  Icon,
  View,
  Text,
  responsiveWidth,
  responsiveHeight,
  Indicator,
} from '@UI';
import useSearch from '../../../Logic/useSearch';
import FastImage from 'react-native-fast-image';
const HomeContent = props => {
  let {allAutoComplete} = props;

  let {allGift, setPage, loading, page} = useSearch();
  return (
    <View style={{flex: 1, backgroundColor: 'white'}} m={5}>
      <FlatList
        ListEmptyComponent={
          <View flex stretch center>
            <Text> sorry no data found with this -_-</Text>
          </View>
        }
        KeyExtractor={index => index.toString()}
        data={allGift}
        // horizontal={true}
        style={{
          alignSelf: 'center',
          backgroundColor: 'white',
        }}
        numColumns={2}
        renderItem={({item, index}) => (
          <View
            key={index}
            // flex
            stretch
            width={45}
            center
            spaceBetween
            // mv={2}
            borderRadius={5}
            bc="grey"
            bw={0.5}
            mv={2}
            mh={2}
            // ph={2}
            // pv={2}
          >
            <FastImage
              style={{width: responsiveWidth(40), height: responsiveHeight(30)}}
              source={{
                uri: item.images.original.url,
                priority: FastImage.priority.normal,
              }}
              resizeMode={FastImage.resizeMode.contain}
            />
            <View center>
              <Text center color="#142546" size={6}>
                {item.title}
              </Text>
            </View>
          </View>
        )}
        ListFooterComponent={
          <View stretch center>
            <Indicator size={20} color="black"></Indicator>
          </View>
        }
        onEndReachedThreshold={0.5}
        onEndReached={() => setPage(page + 1)}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
};

export default HomeContent;
