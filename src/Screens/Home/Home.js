import React, {useEffect, useRef, useState} from 'react';
import {View, Text} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {PagerView} from 'react-native-pager-view';
import Header from '../../Component/Header';
import useSearch from '../../Logic/useSearch';
import HomeContent from './HomeContent/HomeContent';
import SearchContent from './SearchContent/SearchContent';

const HomeScreen = () => {
  const [searchActive, setSearchActive] = useState(false);
  const refViewPager = useRef();

  let {loading, valueSearch, setValueSearch, allAutoComplete} = useSearch();

  useEffect(() => {
    if (searchActive) {
      if (Platform.OS == 'android') refViewPager.current?.setPage(1);
      else refViewPager.current?.setPageWithoutAnimation(1);
    } else {
      if (Platform.OS == 'android') refViewPager.current?.setPage(0);
      else refViewPager.current?.setPageWithoutAnimation(0);
    }
  }, [searchActive]);

  return (
    <View style={{flex: 1}}>
      <Header
        title={'Home'}
        getChanges={textSearch => {
          alert('will send it ');
        }}
        searchActive={searchActive}
        setSearchActive={setSearchActive}
        loading={loading}
        valueSearch={valueSearch}
        setValueSearch={setValueSearch}
      />
      {/* <Text>home here </Text> */}
      <PagerView
        ref={refViewPager}
        style={{flex: 1}}
        initialPage={0}
        transitionStyle={'scroll'}
        overScrollMode={'never'}
        scrollEnabled={false}>
        <View style={{flex: 1, alignSelf: 'stretch'}} key="1">
          <HomeContent/>
        </View>
        <View style={{flex: 1, alignSelf: 'stretch'}} key="2">
          <ScrollView>
            <SearchContent allAutoComplete={allAutoComplete} />
          </ScrollView>
        </View>
      </PagerView>
    </View>
  );
};
export default HomeScreen;
