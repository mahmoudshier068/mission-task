import React, {useEffect, useState} from 'react';
import RecentSearch from './Recent/RecentSearch';
import {FlatList} from 'react-native-gesture-handler';
import {Icon, View, Text} from '@UI';
import {useDispatch} from 'react-redux';
import {ADD_RECENT} from '../../../Action/types';

const SearchContent = props => {
  let {allAutoComplete} = props;
  // useEffect(() => {
  const dispatch = useDispatch();
  // }, [allAutoComplete]);
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View>
        <RecentSearch />
        <Text mh={5} color="black" size={7}>
          Search Result
        </Text>
        <FlatList
          ListEmptyComponent={
            <View flex stretch center>
              <Text> sorry no data found with this -_-</Text>
            </View>
          }
          KeyExtractor={index => index.toString()}
          data={allAutoComplete}
          // horizontal={true}
          style={{
            alignSelf: 'center',
          }}
          renderItem={({item, index}) => (
            <View
              key={index}
              row
              noFlex
              stretch
              width={90}
              spaceBetween
              mv={2}
              ph={2}
              pv={2}
              onPress={() => {
                dispatch({type: ADD_RECENT, payload: item.title});
                alert('saved in RecentSearch');
              }}>
              <View flex>
                <Text color="#142546" size={6}>
                  {item.title}
                </Text>
              </View>
              <Icon
                color="#142546"
                size={6}
                name="right"
                type="AntDesign"
                flip
              />
            </View>
          )}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    </View>
  );
};

export default SearchContent;
