import React, {useEffect} from 'react';
import {PropTypes} from 'prop-types';

import {Pressable, View} from 'react-native';
// import {moderateScale, verticalScale} from 'react-native-size-matters';
// import {styles} from './styles';
import {moderateScale, Text} from '@UI';
import useSearch from '../../../../Logic/useSearch';
import { useSelector } from 'react-redux';
const RecentSearch = props => {
  const {allRecentsSearch} = useSelector(state => ({
    allRecentsSearch: state.recentsSearch.allRecentsSearch,
  }));
  return (
    <View>
      <Text size={7} mh={5} mv={2} color="black">
        Recent Search
      </Text>

      <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
        {allRecentsSearch?.map((item, index) => {
          return (
            // styles().contentKeyWords
            <Pressable
              onPress={() => {
                // setValue(item);
              }}
              key={index}
              android_ripple
              style={{
                backgroundColor: 'red',
                borderRadius: moderateScale(1),
                marginHorizontal: moderateScale(2),
                paddingHorizontal: moderateScale(5),
                paddingVertical: moderateScale(2),
                marginVertical: moderateScale(2),
                backgroundColor: '#f5f4f3',
              }}>
              <Text size={5}>{item}</Text>
            </Pressable>
          );
        })}
      </View>
    </View>
  );
};

export default RecentSearch;
