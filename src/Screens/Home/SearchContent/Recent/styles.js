import {moderateScale} from '@UI';
import { StyleSheet } from 'react-native';

export const styles = () =>
  StyleSheet.create({
    headerText: {
      margin: moderateScale(15),
      fontSize: moderateScale(50) * 0.3,
      color: '#142546',
    },
    contentRecentSearch: {
      width: '100%',
      backgroundColor: 'green',
    },
    contentKeyWords: {
      backgroundColor: '#f5f4f3',
    },
    keyWord: {
      color: '#384151',
      fontSize: moderateScale(20) * 0.3,
    },
  });
