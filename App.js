import * as React from 'react';
import {View, Text, SafeAreaView, StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Routs} from './src/Navigations/Routs';
// import {NativeBaseProvider, StatusBar} from 'native-base';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {persistor, store} from './src/Store';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaView
          style={[
            {flex: 1, alignSelf: 'stretch'},
            {backgroundColor: '#142546'},
          ]}>
          <StatusBar barStyle="light-content" backgroundColor="#142546" />
          <Routs />
        </SafeAreaView>
      </PersistGate>
    </Provider>
  );
}

export default App;
